function Gate8Faker() {};

Gate8Faker.prototype.setTextBoxValue = function(elementId, value){
    var textBox = document.getElementById(elementId);
    textBox.value = value;
};

Gate8Faker.prototype.submitForm = function(elementId){
    var form = document.getElementById(elementId);
    form.submit();
};

document.addEventListener('DOMContentLoaded', function(){
    var gate8Faker = new Gate8Faker();

    var navigationSearch = document.getElementById('search');

    navigationSearch.onkeypress = function handleNavigationSearchKeyPress(e){
        var key=e.keyCode || e.which;

        if (key == 13){
            gate8Faker.setTextBoxValue('q', navigationSearch.value);
            gate8Faker.submitForm('search-form');
        }
    };
});
/*!
 * @name carousel
 * @version 0.1
 * @author pro-verstka@yandex.ru
 * @date 18/11/2013
 */

/*!
 * @template
 *
  <div class="carousel">
    <div class="carousel__prev"></div>
    <div class="carousel__next"></div>
    <div class="carousel__holder">
      <div class="carousel__slider">
        <div class="carousel__item">1</div>
      </div>
    </div>
  </div><!-- /.carousel -->
 */

;(function ($, window, document, undefined) {
  // Create the defaults once
  var pluginName = "carousel",
      defaults = {
        slider: '.carousel__slider',
        item: '.carousel__item',
        holder: '.carousel__holder',
        arrowNext: '.carousel__next',
        arrowPrev: '.carousel__prev',
        showArrows: true,
        speed: 250,
        timeout: 3000,
        offset: 10,
        step: 1,
        fx: 'linear',
        autoslide: false,
        mousewheel: false,
        loop: true,
        //infinite: false,
        callback: null
      };

  function Plugin(element, options) {
    this.element = element;
    this.settings = $.extend( {}, defaults, options );
    this._defaults = defaults;
    this._name = pluginName;
    this.init();
  }

  Plugin.prototype = {
    init: function() {
      var $self = this;
      var $prev = $(this.element).find(this.settings.arrowPrev);
      var $next = $(this.element).find(this.settings.arrowNext);

      var ITEM_TOTAL = $(this.element).find(this.settings.item).length;
      var ITEM_WIDTH = $(this.element).find(this.settings.item).outerWidth(true);
      var SLIDER_WIDTH = ITEM_WIDTH * ITEM_TOTAL;

      $(this.element).find(this.settings.slider).css({
        width: SLIDER_WIDTH + 'px'
      });

      //Hide arrows
      if ( !this.settings.showArrows ) {
        $prev.hide();
        $next.hide();
      };

      if ( this.settings.autoslide ) {
        $self.startTimer(SLIDER_WIDTH, ITEM_WIDTH);
      };

      //Events
      $prev.on('click', function() {
        if ( $self.settings.autoslide ) {
          $self.stopTimer(SLIDER_WIDTH, ITEM_WIDTH);
        }

        $self.movePrev(SLIDER_WIDTH, ITEM_WIDTH);
      });

      $next.on('click', function() {
        if ( $self.settings.autoslide ) {
          $self.stopTimer(SLIDER_WIDTH, ITEM_WIDTH);
        }

        $self.moveNext(SLIDER_WIDTH, ITEM_WIDTH);
      });

      //Mousewheel
      if ( this.settings.mousewheel && $.fn.mousewheel ) {
        $(this.element).on('mousewheel', function(event) {
          if ( event.deltaY == 1 ) {
            if ( $self.settings.autoslide ) {
              $self.stopTimer(SLIDER_WIDTH, ITEM_WIDTH);
            }

            $self.movePrev(SLIDER_WIDTH, ITEM_WIDTH);
          };

          if ( event.deltaY == -1 ) {
            if ( $self.settings.autoslide ) {
              $self.stopTimer(SLIDER_WIDTH, ITEM_WIDTH);
            }

            $self.moveNext(SLIDER_WIDTH, ITEM_WIDTH);
          };

          return false;
        });
      };

      $(window).resize(function() {
        $self.resize(SLIDER_WIDTH);
      });
    },

    slide: function(left, direction, SLIDER_WIDTH, ITEM_WIDTH, callback) {
      $(this.element).find(this.settings.slider).stop().animate({
        left: -left + 'px'
      }, this.settings.speed, this.settings.fx, function() {
        if ( typeof callback == 'function' ) callback();
      });
    },

    movePrev: function(SLIDER_WIDTH, ITEM_WIDTH) {
      var carouselWidth = $(this.element).find(this.settings.holder).width() + this.settings.offset;
      var left = -parseInt( $(this.element).find(this.settings.slider).css('left') );

      if ( this.settings.loop ) {
        if ( left == 0 ) {
          left = SLIDER_WIDTH - carouselWidth;
        } else {
          left -= ITEM_WIDTH * this.settings.step;
        }

        if ( left < 0 ) {
          left = 0;
        }
      } else {
        left -= ITEM_WIDTH * this.settings.step;
        left = ( left < 0 ) ? 0 : left;
      }

      this.slide(left, 'prev', SLIDER_WIDTH, ITEM_WIDTH, this.settings.callback);
    },

    moveNext: function(SLIDER_WIDTH, ITEM_WIDTH) {
      var carouselWidth = $(this.element).find(this.settings.holder).width() + this.settings.offset;
      var left = -parseInt( $(this.element).find(this.settings.slider).css('left') );

      if ( this.settings.loop ) {
        if ( left == SLIDER_WIDTH - carouselWidth ) {
          left = 0;
        } else {
          left += ITEM_WIDTH * this.settings.step;
        }

        if ( left > SLIDER_WIDTH - carouselWidth ) {
          left = SLIDER_WIDTH - carouselWidth;
        }
      } else {
        left += ITEM_WIDTH * this.settings.step;
        left = ( left > SLIDER_WIDTH - carouselWidth ) ? SLIDER_WIDTH - carouselWidth : left;
      }

      this.slide(left, 'next', SLIDER_WIDTH, ITEM_WIDTH, this.settings.callback);
    },

    resize: function(SLIDER_WIDTH) {
      var carouselWidth = $(this.element).find(this.settings.holder).width() + this.settings.offset;
      var left = parseInt( $(this.element).find(this.settings.slider).css('left') );

      if ( left >= SLIDER_WIDTH - carouselWidth ) {
        left = SLIDER_WIDTH - carouselWidth;
      };

      $(this.element).find(this.settings.slider).css({
        left: -left + 'px'
      });
    },

    startTimer: function(SLIDER_WIDTH, ITEM_WIDTH) {
      var $self = this;

      timer = setInterval(function() {
        $self.moveNext(SLIDER_WIDTH, ITEM_WIDTH);
      }, this.settings.timeout);
    },

    stopTimer: function(SLIDER_WIDTH, ITEM_WIDTH) {
      clearInterval(timer);
      this.startTimer(SLIDER_WIDTH, ITEM_WIDTH);
    }
  };

  $.fn[pluginName] = function(options) {
    return this.each(function() {
      if ( !$.data(this, "plugin_" + pluginName) ) {
        $.data(this, "plugin_" + pluginName, new Plugin(this, options));
      }
    });
  };
})(jQuery, window, document);
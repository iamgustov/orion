/*!
 * Moonkake v3.0.5
 *
 * http://moonkake.ru
 */

/* EASING ANIMATION
-------------------------------------------------- */

$.extend(
  $.easing, {
    easeOutExpo: function (x, t, b, c, d) {return (t==d) ? b+c : c * (-Math.pow(2, -10 * t/d) + 1) + b;},
    easeInOutCubic: function (x, t, b, c, d) {if ((t/=d/2) < 1) return c/2*t*t*t + b; return c/2*((t-=2)*t*t + 2) + b;}
  }
);

/* VAR
-------------------------------------------------- */

var $document = $(document);
var $window = $(window);

var isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent);

var SPEED = 500;
var TIMEOUT = 5000;
var FX = (isMobile) ? 'linear' : 'easeInOutCubic';
var OFFSET = 0;

/* CAROUSEL
-------------------------------------------------- */

if ( $.fn.carousel ) {

  $('.carousel').carousel({
    offset: 14
  });

  $document.on('click', '.carousel-gallery-thumb .carousel__item', function() {
    var src = $(this).data('medium');
    $(this).closest('.carousel').prev('.gallery').find('img').attr('src', src);
    return false;
  });

};

/* CHECKBOX & RADIO
-------------------------------------------------- */

if ( $.fn.berry ) {

  $('.berry').berry();

};

/* CHOSEN
-------------------------------------------------- */

if ( $.fn.chosen ) {

  $('.chosen-select').chosen({
    disable_search: true
  });

};

/* TABS
-------------------------------------------------- */

$document.on('click', '.tabs .tab__title .tab__item', function() {
  var $root = $(this).closest('.tabs');
  var index = $(this).index();

  $root.find('.tab__title .tab__item').eq( index ).addClass('-active').siblings('div').removeClass('-active');
  $root.find('.tab__content .tab__item').eq( index ).addClass('-active').siblings('div').removeClass('-active');
});

/* UI
-------------------------------------------------- */

/* DATEPICKER */

$.datepicker.regional['ru'] = {
  closeText: 'Закрыть',
  prevText: '&#x3c;Пред',
  nextText: 'След&#x3e;',
  currentText: 'Сегодня',
  monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
  monthNamesShort: ['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
  dayNames: ['воскресенье','понедельник','вторник','среда','четверг','пятница','суббота'],
  dayNamesShort: ['вск','пнд','втр','срд','чтв','птн','сбт'],
  dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
  weekHeader: 'Не',
  dateFormat: 'dd.mm.yy',
  firstDay: 1,
  isRTL: false,
  showMonthAfterYear: false,
  yearSuffix: ''
};

$.datepicker.setDefaults($.datepicker.regional['ru']);

$('.input-date').datepicker();

/* RANGE */

var $range = $('.range__slider');
var min = $range.data('min');
var max = $range.data('max');
var start = $range.data('start');
var end = $range.data('end');

$('.range__slider').slider({
  range: true,
  min: min,
  max: max,
  values: [start, end],
  slide: function(event, ui) {
    $('.range__from').html('Минимальная цена: <span>'+ ui.values[0] +' р.</span>');
    $('.range__to').html('Максимальная цена: <span>'+ ui.values[1] +' р.</span>');

    $('#price_from').val(ui.values[0]);
    $('#price_to').val(ui.values[1]);
  }
});

$('.range__from').html('Минимальная цена: <span>'+ $('.range__slider').slider('values', 0) +' р.</span>');
$('.range__to').html('Максимальная цена: <span>'+ $('.range__slider').slider('values', 1) +' р.</span>');

$('#price_from').val($('.range__slider').slider('values', 0));
$('#price_to').val($('.range__slider').slider('values', 1));

/* TOGGLER
-------------------------------------------------- */

$('.js-toggler').on('click', function() {
  var $root = $(this).closest('.js-root');
  var $target = $root.find('.js-target:first');

  if ( $target.is(':hidden') ) {
    $target.slideDown( SPEED/3, FX );
    $root.addClass('-opened');
  } else {
    $target.slideUp( SPEED/3, FX );
    $root.removeClass('-opened');
  }

  return false;
});

/* TYPE
-------------------------------------------------- */

$('.type').on('click', '.list__item', function() {
  $(this).addClass('-active').siblings('.list__item').removeClass('-active');
  return false;
});

/* TAG
-------------------------------------------------- */

/* REMOVE TAG */

$document.on('click', '.tag span', function() {
  var $self = $(this);
  var id = $self.closest('.tag').data('id');

  $self.closest('.tag').remove();

  $('.filter-advanced .berry-input input[id="'+ id +'"]')
    .prop('checked', false)
    .closest('.berry-input')
    .removeClass('-berry-checked');
});

/* ADD TAG */

$('.filter-advanced .berry-input').on('change', function() {
  var $self = $(this);
  var label = $self.find('label').text();
  var id = $self.find('input').attr('id');

  if ( $self.find('input').prop('checked') ) {
    $('.filter__tag').append('<li class="list__item tag" data-id="'+ id +'">'+ label +' <span>x</span></li>');
  } else {
    $('.filter__tag').find('.tag[data-id="'+ id +'"]').remove();
  };
});

/* DIALOG
-------------------------------------------------- */

function openDialog(src) {
  $('.dialog-content-inner').html('').load( src , function() {
  //$('.dialog-content-inner').load( src , function() {
    $('.berry').berry();
    $('.chosen-select').chosen({disable_search: true});
    $('.carousel').carousel({offset: 14});
    $('.input-date').datepicker();

      var strDateStart = $('#good-date-from').val();
      var strDateEnd = $('#good-date-to').val();
      var days = getDateRange(strDateStart, strDateEnd);

      var pricePerDay = $('#price_per_day').val();
      $('.good-price__total .good-price__total_span').html((days * pricePerDay) + ' р.');
      $('.good-price__total .good-price__total_span10').html((days * pricePerDay / 10) + ' р.');


      var placeId = $('#place_id').val();
      var people = $('#good-people').val();

      var regExpStart = strDateStart.match(/\d+/gim);
      var dateStart = new Date (regExpStart[2], regExpStart[1], regExpStart[0]);
      var regExpEnd = strDateEnd.match(/\d+/gim);
      var dateEnd = new Date (regExpEnd[2], regExpEnd[1], regExpEnd[0]);

      var path = '/fast-order/' + placeId + '/' + dateStart.getTime() + '/' + dateEnd.getTime() + '/' + people;
      $('#fast-order').attr('data-src', path);

    $('body').addClass('-locked');
    $('.dialog').fadeIn(SPEED/2);
  });
};

$document.on('click', '.js-dialog', function() {
  var src = $(this).data('src');
  openDialog(src);
  return false;
});

$('.dialog-close').on('click', function() {
  $('.dialog').fadeOut(SPEED/2);
  $('body').removeClass('-locked');
});

/* PAGE CONTROL
-------------------------------------------------- */

var $section = $('.main .section');
var $map = $('.main .map');

$('.page-control__left').on('click', function() {
  $section
    .css({
      overflow: 'hidden',
      zIndex: '-1'
    })
    .animate({
      width: 0,
      opacity: 0
    }, SPEED/2);

  $map
    .animate({
      width: '100%'
    }, SPEED/2, FX, function() {
      google.maps.event.trigger(map, 'resize');
    });
});

$('.page-control__right').on('click', function() {
  $section
    .animate({
      opacity: 1,
      width: '56%'
    }, SPEED/2)
    .css({
      overflow: 'visible',
      zIndex: '1'
    });

  $map
    .animate({
      width: '44%'
    }, SPEED/2, FX, function() {
      google.maps.event.trigger(map, 'resize');
    });
});

/* GOOD CALCULATOR
 -------------------------------------------------- */

function getDateRange(strDateStart, strDateEnd) {
    var regExpDateStart = strDateStart.match(/\d+/gim);
    var regExpDateEnd = strDateEnd.match(/\d+/gim);

    var dateStart = new Date (regExpDateStart[2], regExpDateStart[1], regExpDateStart[0]);
    var dateEnd = new Date(regExpDateEnd[2], regExpDateEnd[1], regExpDateEnd[0]);
    var dateRange = Math.floor((dateEnd - dateStart)/(1000*60*60*24)) + 1;

    return dateRange;
};

$document.on('change', '.good-order input', function() {
    var strDateStart = $('#good-date-from').val();
    var strDateEnd = $('#good-date-to').val();
    var days = getDateRange(strDateStart, strDateEnd);

    var pricePerDay = $('#price_per_day').val();
    $('.good-price__total .good-price__total_span').html((days * pricePerDay) + ' р.');
    $('.good-price__total .good-price__total_span10').html((days * pricePerDay / 10) + ' р.');

    var placeId = $('#place_id').val();
    var people = $('#good-people').val();

    var regExpStart = strDateStart.match(/\d+/gim);
    var dateStart = new Date (regExpStart[2], regExpStart[1], regExpStart[0]);
    var regExpEnd = strDateEnd.match(/\d+/gim);
    var dateEnd = new Date (regExpEnd[2], regExpEnd[1], regExpEnd[0]);

    var path = '/fast-order/' + placeId + '/' + dateStart.getTime() + '/' + dateEnd.getTime() + '/' + people;
    $('#fast-order').attr('data-src', path);
});

$document.on('change', '#good-people', function() {
    var strDateStart = $('#good-date-from').val();
    var strDateEnd = $('#good-date-to').val();

    var placeId = $('#place_id').val();
    var people = $('#good-people').val();

    var regExpStart = strDateStart.match(/\d+/gim);
    var dateStart = new Date (regExpStart[2], regExpStart[1], regExpStart[0]);
    var regExpEnd = strDateEnd.match(/\d+/gim);
    var dateEnd = new Date (regExpEnd[2], regExpEnd[1], regExpEnd[0]);

    var path = '/fast-order/' + placeId + '/' + dateStart.getTime() + '/' + dateEnd.getTime() + '/' + people;
    $('#fast-order').attr('data-src', path);
});

function validateEmail(email){
    var pattern = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;
    return pattern.test(email);
}

function validatePhoneNumber(phone){
    var pattern = /^[0-9-+]+$/;
    return pattern.test(phone);
}

$document.on('click', '#send-payment', function() {
    var email = $('#email').val(),
        firstName = $('#firstname').val(),
        lastName = $('#lastname').val(),
        phone = $('#phone').val();
    var flag = true;

    if (lastName.trim() == '') {
        flag = false;
        $('.form__error').show();
        $('.form__error__item').html('Введите корректную фамилию..');
        $('#lastname').focus();
    } else if (firstName.trim() == '') {
        flag = false;
        $('.form__error').show();
        $('.form__error__item').html('Введите корректное имя..');
        $('#firstname').focus();
    } else if (!validatePhoneNumber(phone)) {
        flag = false;
        $('.form__error').show();
        $('.form__error__item').html('Введите номер телефона в формате +79xxxxxxxxx..');
        $('#phone').focus();
    } else if (!validateEmail(email)) {
        flag = false;
        $('.form__error').show();
        $('.form__error__item').html('Введите корректный email..');
        $('#email').focus();
    }

    if (flag)
        $('#z-payment').submit();
});
class OrderMailer < ActionMailer::Base
  default from: "info@roomscanner.ru"

  def order_email(email, message)
    @url  = 'http://roomscanner.ru'
    @body = message
    mail(to: email, subject: 'Спасибо за заказ!')
  end
end

class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  #protect_from_forgery with: :exception
  protect_from_forgery with: :null_session

  private

  def search_places filter

    if filter
      flag  = false

      if filter[:prices]
        places = Place.where(price_per_day: {'$gte' => filter[:prices][0], '$lte' => filter[:prices][1]})
        flag = true
      end

      if filter[:q]
        if flag
          places = places.where(name: filter[:q])
        else
          places = Place.where(name: filter[:q])
          flag = true
        end
      end

      if filter[:capacity]
        if flag
          places = places.where(capacity: {'$gte' => filter[:capacity]})

        else
          places = Place.where(capacity: {'$gte' => filter[:capacity]})
          flag = true
        end
      end

      if filter[:facilities]
        facilities = Facility.where(:outer_id.in => filter[:facilities])

        facilities.each do |facility|
          if flag
            places = places.where(:id.in => facility.place_ids)
          else
            places = Place.where(:id.in => facility.place_ids)
            flag = true
          end
        end
      end

      if filter[:languages]
        owner_languages = OwnerLanguage.where(:outer_id.in => filter[:languages])

        owner_languages.each do |owner_language|
          if flag
            places = places.where(:id.in => owner_language.place_ids)
          else
            places = Place.where(:id.in => owner_language.place_ids)
            flag = true
          end
        end
      end

      # TODO place type
      #if filter[:capacity]
      #  if flag
      #    places = places.where(capacity: {'$gte' => filter[:capacity]})
      #  else
      #    places = Place.where(capacity: {'$gte' => filter[:capacity]})
      #    flag = true
      #  end
      #end
    end

    #throw filter
    #throw places.count

    places_list = []

    places.each do |place|
      image =  place.image_links.first if place.image_links
      places_list.push({id: place.outer_id, name: place.name, teaser: place.teaser, price_per_day: place.price_per_day, image: image, location: place.location})
    end

    places_list

  end

  def search_places_by_name name
    places = Place.where(name: name)

    places_list = []

    places.each do |place|
      image =  place.image_links.first if place.image_links
      places_list.push({id: place.outer_id, name: place.name, teaser: place.teaser, price_per_day: place.price_per_day, image: image})
    end

    places_list
  end

  def search_places_by_price from, to
    places = Place.where(price_per_day: {'$gte' => from, '$lte' => to})

    places_list = []

    places.each do |place|
      image =  place.image_links.first if place.image_links
      places_list.push({id: place.outer_id, name: place.name, teaser: place.teaser, price_per_day: place.price_per_day, image: image})
    end

    places_list
  end

  def get_places_teaser_list
    places = Place.all
    places_list = []

    places.each do |place|
      image =  place.image_links.first if place.image_links
      places_list.push({id: place.outer_id, name: place.name, teaser: place.teaser, price_per_day: place.price_per_day, image: image, location: place.location})
    end

    places_list
  end

  def get_place_fast_order_info id
    place = Place.where(outer_id: id).first
    place_fast_order_info = nil

    if place
      image =  place.image_links.first if place.image_links
      place_fast_order_info = {id: place.outer_id, name: place.name, teaser: place.teaser, price_per_day: place.price_per_day, image: image}
    end

    place_fast_order_info
  end

  def parse_place_type parser_results
    PlaceType.delete_all

    if parser_results.has_key?('types')
      types = parser_results['types'].first

      if types.has_key?('type')
        types = types['type']

        types.each do |type|
          place_type = PlaceType.new
          place_type.name = type['name'] if type['name']
          place_type.outer_id = type['id'] if type['id']
          place_type.save!
        end
      end
    end
  end

  def parse_facility parser_results
    Facility.delete_all

    if parser_results.has_key?('types')
      types = parser_results['types'].first

      if types.has_key?('type')
        types = types['type']

        types.each do |type|
          facility = Facility.new
          facility.name = type['name'] if type['name']
          facility.outer_id = type['id'] if type['id']
          facility.save!
        end
      end
    end
  end

  def parse_owner_language parser_results
    OwnerLanguage.delete_all

    if parser_results.has_key?('types')
      types = parser_results['types'].first

      if types.has_key?('type')
        types = types['type'].first

        if types.has_key?('type')
          types = types['type']

          types.each do |type|
            owner_language = OwnerLanguage.new
            owner_language.name = type['name'] if type['name']
            owner_language.outer_id = type['id'] if type['id']
            owner_language.save!
          end
        end
      end
    end
  end

  def parse_region parser_results
    City.delete_all
    Region.delete_all

    if parser_results.has_key?('types')
      types = parser_results['types'].first

      if types.has_key?('type')
        types = types['type'].first

        if types.has_key?('type')
          types = types['type']
          types.each do |region|
            r = Region.new

            r.name = region['name'] if region['name']
            r.outer_id = region['id'].to_i if region['id']

            r.save!

            if region.has_key?('type')
              cities = region['type']

              cities.each do |city|
                c = City.new
                c.name = city['name'] if city['name']
                c.outer_id = city['id'].to_i if city['id']

                c.save!

                r.cities.push c
              end
            end

            r.save!
          end
        end
      end
    end
  end

  def parse_place parser_results
    Place.delete_all

    if parser_results.has_key?('places')
      places = parser_results['places']

      places.each do |place|
        if place.has_key?('place')
          place = place['place']

          place.each do |place_term|
            pl = Place.new
            pl.name = place_term['zagolovok'].first if place_term['zagolovok']
            pl.capacity = place_term['vmestimost'].first if place_term['vmestimost']
            pl.description = place_term['description'].first if place_term['description']
            pl.outer_id = place_term['id'].first.to_i if place_term['id']
            pl.address = place_term['street'].first + ', ' + place_term['dom'].first if place_term['street'] && place_term['dom']
            pl.price_per_month = place_term['cost1'].first.to_f if place_term['cost1']
            pl.price_per_week = place_term['cost2'].first.to_f if place_term['cost2']
            pl.price_per_day = place_term['cost3'].first.to_f if place_term['cost3']
            pl.price_per_guest = place_term['cost4'].first.to_f if place_term['cost4']
            pl.house_rules = place_term['pravila'].first if place_term['pravila']
            pl.teaser = place_term['kratkoe'].first if place_term['kratkoe']

            location = []
            location = place_term['geocode'].first if place_term['geocode']

            if location
              location = location.split(',')

              pl.push location: location[0]
              pl.push location: location[1]
            end

            photos = []
            photos = place_term['photos'].first()['photo'] if place_term['photos']

            photos.each do |photo|
              pl.push(image_links: photo['content'])
            end if photos

            pl.save!

            facilities = []
            facilities = place_term['facilities'].first()['term'] if place_term['facilities']
            place_facilities = []

            facilities.each do |facility|
              facility = facility['id']
              place_facilities.push facility.to_i
            end if facilities

            pl.facilities = Facility.where(:outer_id.in => place_facilities)

            pl.save!

            languages = []
            languages = place_term['leng'].first if place_term['leng']

            pl.owner_languages = OwnerLanguage.where(outer_id: languages) if languages.size > 0
            pl.save!
          end
        end
      end
    end
  end
end

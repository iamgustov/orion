class MapController < ApplicationController
  before_filter :get_owner_languages, only: [:index]
  before_filter :get_facilities, only: [:index]
  before_filter :get_place_types, only: [:index]

  def index
    @places = get_places_teaser_list
    @owner_languages_list = @owner_languages.map { |language| {id: language.outer_id, name: language.name} }
    @facility_list = @facilities.map { |facility| {id: facility.outer_id, name: facility.name} }
    @place_types_list = @place_types.map { |place_type| {id: place_type.outer_id, name: place_type.name} }
    @prices_per_day = {
        min: Place.min(:price_per_day),
        max: Place.max(:price_per_day)
    }
  end

  def payment
    render text: '334BBF1AE4FB009063FF9F881986DEC2'
  end

  private

  def get_owner_languages
    @owner_languages = OwnerLanguage.all
  end

  def get_facilities
    @facilities = Facility.all
  end

  def get_place_types
    @place_types = PlaceType.all
  end

end

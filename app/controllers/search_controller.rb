class SearchController < ApplicationController
  before_filter :get_owner_languages, only: [:index]
  before_filter :get_facilities, only: [:index]
  before_filter :get_place_types, only: [:index]

  def index
    f_capacity = params[:people] if params[:people]
    f_prices  = [params[:price_from], params[:price_to]] if params[:price_from] && params[:price_to]
    f_q = params[:q].strip if params[:q]
    f_place_types = params[:l][:pt] if params[:l]
    f_facilities = params[:l][:f] if params[:l]
    f_languages = params[:l][:lan] if params[:l]

    filter = {
        capacity: f_capacity,
        prices: f_prices,
        q: /#{f_q}/i,
        place_types: f_place_types,
        facilities: f_facilities,
        languages: f_languages
    }

    @places = search_places filter

    @owner_languages_list = @owner_languages.map { |language| {id: language.outer_id, name: language.name} }
    @facility_list = @facilities.map { |facility| {id: facility.outer_id, name: facility.name} }
    @place_types_list = @place_types.map { |place_type| {id: place_type.outer_id, name: place_type.name} }
    @prices_per_day = {
        min: Place.min(:price_per_day),
        max: Place.max(:price_per_day)
    }

    render 'map/index'
  end

  private

  def get_owner_languages
    @owner_languages = OwnerLanguage.all
  end

  def get_facilities
    @facilities = Facility.all
  end

  def get_place_types
    @place_types = PlaceType.all
  end
end

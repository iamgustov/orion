class OrderController < ApplicationController
  def fast
    @place = get_place_fast_order_info params[:id]
    @date_from = params[:datefrom]
    @date_to = params[:dateto]
    @people_count = params[:people]

    render file: 'popup/payment', layout: false
  end

  def send_mail

    last_name = params[:lastname] if params[:lastname]
    first_name = params[:firstname] if params[:firstname]
    #city = params[:city] if params[:city]
    #area = params[:area] if params[:area]
    phone = params[:phone] if params[:phone]
    email = params[:email] if params[:email]
    date_from = params[:date_from] if params[:date_from]
    date_to = params[:date_to] if params[:date_to]
    people = params[:people] if params[:people]
    id = params[:id] if params[:id]
    order_number = params[:LMI_SYS_INVS_NO] if params[:LMI_SYS_INVS_NO]
    transaction_date = params[:LMI_SYS_TRANS_DATE] if params[:LMI_SYS_TRANS_DATE]

    place = Place.where(outer_id: id).first if id

    price = (((date_to.to_f - date_from.to_f)/ (1000*60*60*24)).floor + 1) * place.price_per_day.to_i if place && place.price_per_day

    date_from = Time.at(date_from.to_i/1000).to_date.strftime('%d.%m.%Y') if date_from
    date_to = Time.at(date_to.to_i/1000).to_date.strftime('%d.%m.%Y') if date_to

    customer_message = []
    customer_message.push "Уважаемый, #{first_name} #{last_name}, Вы оплатили бронирование места:"
    customer_message.push "#{place.name} с #{date_from} по #{date_to}"
    customer_message.push "Кол-во человек: #{people}"
    customer_message.push "Полная стоимость проживания: #{price}"
    customer_message.push "К оплате за бронирование: #{(price/10)}"
    customer_message.push "Оплата от #{transaction_date} № #{order_number}"
    customer_message.push "С Вами свуяжутся наши консультанты в течении 24 часов."

    admin_message = []
    admin_message.push "Был сделан заказ на бронирование места #{place.name} с #{date_from} по #{date_to}"
    admin_message.push "Данные заказчика: #{first_name} #{last_name}, #{phone}"
    admin_message.push "Оплата от #{transaction_date} № #{order_number}"
    admin_message.push "Кол-во человек: #{people}"

    res = OrderMailer.order_email('admin@patriotdom.ru', admin_message).deliver
    res = OrderMailer.order_email('me.antony.rain@gmail.com', admin_message).deliver
    res = OrderMailer.order_email(email, customer_message).deliver if email

    redirect_to controller: :map, action: :index
  end
end

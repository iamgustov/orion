class PlacesController < ApplicationController
  before_filter :get_place, only: [:show]
  before_filter :get_facilities, only: [:show]

  def show
    facility_list = @facilities.map { |f| f.name}
    facilities = @place.facilities.map { |facility| facility.name }
    total_failities = []

    facility_list.each do |f|
      status = (facilities.include? f) ? 1 : 0
      total_failities.push({name: f, status: status})
    end

    @place = {
            id: @place.outer_id,
            name: @place.name,
            description: @place.description,
            rules: @place.house_rules,
            teaser: @place.teaser,
            address: @place.address,
            price_per_day: @place.price_per_day,
            facilities: total_failities,
            images: @place.image_links
        };

        #@place.facilities.map{|facility| {name: facility.name, id: facility.id}}

    render 'popup/good', layout: false
  end

  private

  def get_place
    @place = Place.where(outer_id: params[:id]).first

    render 'layouts/error404', layout: false if !@place
  end

  def get_facilities
    @facilities = Facility.all
  end
end

require 'net/http'
require 'xmlsimple'

class ParserController < ApplicationController
  before_filter :get_api_paths, only: [:place_type, :facility, :owner_language, :place, :region, :scope]

  def place_type
    xml_data = Net::HTTP::Proxy('proxy', '3128').get_response(URI.parse(@place_type_url)).body
    #xml_data = Net::HTTP.get_response(URI.parse(@place_type_url)).body
    parser_results = XmlSimple.xml_in(xml_data)

    parse_place_type parser_results

    @result = PlaceType.all.to_a

    render 'parser/index' if @flag
  end

  def facility
    xml_data = Net::HTTP::Proxy('proxy', '3128').get_response(URI.parse(@facility_url)).body
    #xml_data = Net::HTTP.get_response(URI.parse(@facility_url)).body
    parser_results = XmlSimple.xml_in(xml_data)

    parse_facility parser_results

    @result = Facility.all.to_a

    render 'parser/index' if @flag
  end

  def owner_language
    xml_data = Net::HTTP::Proxy('proxy', '3128').get_response(URI.parse(@owner_language_url)).body
    #xml_data = Net::HTTP.get_response(URI.parse(@owner_language_url)).body
    parser_results = XmlSimple.xml_in(xml_data)

    parse_owner_language parser_results

    @result = OwnerLanguage.all.to_a

    render 'parser/index' if @flag
  end

  def place
    xml_data = Net::HTTP::Proxy('proxy', '3128').get_response(URI.parse(@place_url)).body
    #xml_data = Net::HTTP.get_response(URI.parse(@place_url)).body
    parser_results = XmlSimple.xml_in(xml_data)

    @result = parse_place parser_results

    @result = Place.all.to_a

    render 'parser/index' if @flag
  end

  def region
    xml_data = Net::HTTP::Proxy('proxy', '3128').get_response(URI.parse(@place_url)).body
    #xml_data = Net::HTTP.get_response(URI.parse(@region_url)).body
    parser_results = XmlSimple.xml_in(xml_data)

    parse_region parser_results

    @result = Region.all.to_a

    render 'parser/index' if @flag
  end

  def scope
    @flag = !@flag

    place_type
    facility
    owner_language
    region

    @flag = !@flag
    place
  end

  private

  def get_api_paths
    @place_type_url = 'http://194.58.98.60/modules/m_roomscaner.php?get_xml=Y&dictonary_type=realty&prefix=boxreg&token=JHhferuhu_fnekj_4r3feergh3u4th84t'
    @facility_url  = 'http://194.58.98.60/modules/m_roomscaner.php?get_xml=Y&dictonary_type=facility&prefix=boxreg&token=JHhferuhu_fnekj_4r3feergh3u4th84t'
    @owner_language_url = 'http://194.58.98.60/modules/m_roomscaner.php?get_xml=Y&dictonary_type=leng&prefix=boxreg&token=JHhferuhu_fnekj_4r3feergh3u4th84t'
    @place_url = 'http://194.58.98.60/modules/m_roomscaner.php?get_xml=Y&prefix=boxreg&token=JHhferuhu_fnekj_4r3feergh3u4th84t'
    @region_url = 'http://194.58.98.60/modules/m_roomscaner.php?get_xml=Y&dictonary_type=region&prefix=boxreg&token=JHhferuhu_fnekj_4r3feergh3u4th84t'

    @flag = true
  end
end

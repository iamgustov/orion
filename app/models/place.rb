class Place
  include Mongoid::Document
  include Mongoid::Timestamps

  field :name, type: String
  field :description, type: String
  field :address, type: String
  field :price_per_month, type: Float
  field :price_per_week, type: Float
  field :price_per_day, type: Float
  field :price_per_guest, type: Float
  field :house_rules, type: String
  field :outer_id, type: Integer
  field :teaser, type: String
  field :image_links, type: Array
  field :capacity, type: Integer
  field :location, type: Array

  #validates :name, :description, presence: true
  validates :outer_id, uniqueness: true#, presence: true

  has_and_belongs_to_many :place_types
  has_and_belongs_to_many :facilities
  has_and_belongs_to_many :owner_languages
end

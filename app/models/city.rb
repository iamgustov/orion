class City
  include Mongoid::Document
  include Mongoid::Timestamps

  field :name, type: String
  field :outer_id, type: Integer

  validates :name, presence: true, uniqueness: true
  validates :outer_id, uniqueness: true #, presence: true

  belongs_to :region
end

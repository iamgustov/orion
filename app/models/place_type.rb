class PlaceType
  include Mongoid::Document
  include Mongoid::Timestamps

  field :name, type: String
  field :outer_id, type: Integer

  validates :name, presence: true, uniqueness: true
  validates :outer_id, uniqueness: true #, presence: true

  has_and_belongs_to_many :places
end

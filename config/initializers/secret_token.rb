# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
Orion::Application.config.secret_key_base = 'e0f84f088f402091a893bd47ba4e2419e056305787c1de3d06715770d66a250c560ff0050dd0fc3c0379f3ab581013196b82cf17fb2b933f2291ee9ea0131f46'

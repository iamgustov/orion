Orion::Application.routes.draw do
  root 'map#index'

  #get '/places/:id', to: 'places#show', as: 'single_place'

  get '/show(/:id)', to: 'places#show'
  get '/fast-order/:id/:datefrom/:dateto/:people', to: 'order#fast'
  get '/search', to: 'search#index'
  get '/ZP61445590.HTML', to: 'map#payment'
  post '/send', to: 'order#send_mail'

  # here the path for parse scope
  scope '/parse' do
    #get '/place-types', to: 'parser#place_type'
    #get '/facility', to: 'parser#facility'
    #get '/owner-language', to: 'parser#owner_language'
    #get '/place', to: 'parser#place'
    #get '/region', to: 'parser#region'
    get '/scope', to: 'parser#scope'
  end
end
